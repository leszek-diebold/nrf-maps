/*
 * © Philips Lighting, 2018.
 *   All rights reserved.
 */

#import <UIKit/UIKit.h>

/*!
 *  Represents an anchor object thats holds the geograpical location with the corresponding two-dimensional
 *  cartesian point.
 */
@interface IMAnchor : NSObject

/*!
 *  The x-coordinate of the point.
 */
@property (nonatomic, readonly) CGFloat x;

/*!
 *  The y-coordinate of the point.
 */
@property (nonatomic, readonly) CGFloat y;

/*!
 *  The longitude of the geographical location.
 */
@property (nonatomic, readonly) double lon;

/*!
 *  The latitude of the geographical location.
 */
@property (nonatomic, readonly) double lat;

/*!
 *  Returns an initialized anchor object with the specified x, y and lon, lat.
 *  @param x      The x-coordinate of the point.
 *  @param y      The y-coordinate of the point.
 *  @param lon    The longitude of the geographical location.
 *  @param lat    The latitude of the geographical location.
 */
- (nonnull instancetype)initWithX:(CGFloat)x y:(CGFloat)y lon:(double)lon lat:(double)lat;

@end



