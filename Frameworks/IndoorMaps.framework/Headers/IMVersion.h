/*
 * © Philips Lighting, 2018.
 *   All rights reserved.
 */

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT int IMGetVersionMajor(void);
FOUNDATION_EXPORT int IMGetVersionMinor(void);
FOUNDATION_EXPORT int IMGetVersionFix(void);

FOUNDATION_EXPORT NSString * IMGetBuildDate(void);
FOUNDATION_EXPORT NSString * IMGetBuildTime(void);
