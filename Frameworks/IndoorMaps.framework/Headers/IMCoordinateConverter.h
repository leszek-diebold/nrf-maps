/*
 * © Philips Lighting, 2018.
 *   All rights reserved.
 */

#import <IndoorMaps/IMAnchor.h>
#import <IndoorMaps/IMLocation.h>

/*!
 *  A utility object that converts between WGS84 and two-dimensional cartesian coordinate system.
 *  This class can be used to georeference a floor plan image.
 */
@interface IMCoordinateConverter : NSObject

/*!
 *  The angle of the true north measured in radians when north is towards the top of a
 *  two-dimensional depiction.
 */
@property (nonatomic, readonly) double northAngle;

/*!
 *  Indicates the number of points within a unit of 1 meter
 */
@property (nonatomic, readonly) double ppm;

/*!
 *  Returns an initialized IMCoordinateConverter object given the anchors that this class will
 *  use for converting.
 *  @param first    The first anchor.
 *  @param second   The second anchor.
 */
- (nonnull instancetype)initWithFirstAnchor:(nonnull IMAnchor *)first secondAnchor:(nonnull IMAnchor *)second;

/*!
 *  Returns the point that corresponds to the latitude and longitude
 *  @param longitude   The longitude of the geographical location.
 *  @param latitude    The latitude of the geographical location.
 */
- (CGPoint)pointFromLongitude:(double)longitude andLatitude:(double)latitude;

/*!
 *  Returns the geograpical location that corresponds to x and y coordinate of a point.
 *  @param x    The x-coordinate of the point.
 *  @param y    The y-coordinate of the point.
 */
- (nonnull IMLocation *)locationFromX:(double)x andY:(double)y;

@end


