/*
 * © Philips Lighting, 2018.
 *   All rights reserved.
 */

#import <Foundation/Foundation.h>

/*!
 *  Represents an indoor geographical location using WGS84 coordinates in double precision along
 *  with a floorlevel.
 */
@interface IMLocation : NSObject

/*!
 *  Returns an initialized location object with the specified longitude, latitude and floorlevel.
 *  @param lon          The longitude of the location.
 *  @param lat          The latitude of the location.
 *  @param floorLevel   The floor level of the location. Typically logical levels are used when
 *                      generating maps for venues.
 */
- (nonnull instancetype)initWithLon:(double)lon lat:(double)lat floorLevel:(int)floorLevel;

/*!
 *  Returns an initialized location object with the specified longitude and latitude, floor level will be invalid.
 *  set to an invalid number.
 *  @param lon          The longitude of the location.
 *  @param lat          The latitude of the location.
 */
- (nonnull instancetype)initWithLon:(double)lon lat:(double)lat;

/*!
 *  A Boolean value that indicates whether the location is created with a (valid) floor level.
 */
- (BOOL)hasFloorLevel;

/*!
 *  Horizontal distance between this value and location in meters.
 *  This method uses a flat earth approximation and should not be used for large distances.
 *  @param lon     The longitude of the location.
 *  @param lat     The latitude of the location.
 */
- (double)distanceToLon:(double)lon lat:(double)lat;

/*!
 *  The longitude of the location.
 */
@property (nonatomic, readonly) double lon;

/*!
 *  The latitude of the location
 */
@property (nonatomic, readonly) double lat;

/*!
 *  The floor level of the location. Typically logical levels are used when
 *  generating maps for venues.
 */
@property (nonatomic, readonly) int floorLevel;

@end
