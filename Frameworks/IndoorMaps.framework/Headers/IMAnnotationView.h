/*
 * © Philips Lighting, 2018.
 *   All rights reserved.
 */

#import <UIKit/UIKit.h>
#import <IndoorMaps/IMLocation.h>

/*!
 *  An object that manages the content for a rectangular area on a specific map location.
 */
@interface IMAnnotationView : UIView

/*!
*  The center point (specified as a map coordinate) of the annotation.
*/
@property (nonnull, nonatomic) IMLocation *location;

/*!
 *  When set to false the orientation of the annotion view will rotate along with the map rather
 *  then a static orientation against the device screen.
 */
@property (nonatomic, assign) BOOL counterRotate;

/*!
 *  The value that you specify and can use to identify this annotation view inside your application.
 */
@property (nullable, nonatomic, strong) NSString *identifier;

/*!
 *  Sets a constant metric size of the annotation, when zooming the map the screen size of the annotion will
 *  also change accordingly. Default this property has the CGSizeZero value using the default frame.
 */
@property (nonatomic, assign) CGSize metricSize;

/*!
 *  Returns an initialized annotation view object with the specified location as center point. By default it is oriented
 *  against the device screen.
 *  @param size     The frame size of the annotation.
 *  @param location The center point (specified as a map coordinate) of the annotation.
 */
- (nonnull instancetype)initWithFrameSize:(CGSize)size location:(nonnull IMLocation *)location;

@end
