/*
 * © Philips Lighting, 2018.
 *   All rights reserved.
 */

#import <UIKit/UIKit.h>
#import <IndoorMaps/IMAnnotationView.h>

@protocol IMMapViewControllerDelegate;

/*!
 *  A controller that can be used to visualize indoor map files.
 *   - Flick and pinch gestures for scrolling around the map and zooming in and out.
 *   - Floor switching for multifloor venues.
 *   - Display of customizable annotations.
 *   - User location visualization with tracking.
 *   - Navigation to arbitrary locations.
 */
@interface IMMapViewController : UIViewController

/*!
 *  The delegate is used to communicate map view controller events.
 */
@property (nullable, nonatomic, weak) id<IMMapViewControllerDelegate> delegate;

/*!
 *  Zoom distance in meters when in user tracking mode.
 */
@property (nonatomic, assign) double userTrackingZoomDistance;

/*!
 *  Load a floor plan file. Returns false when the floorplan can't be read.
 *  @param path    The path that indoor map file.
 *  @param error   A pointer to an error object, you may specify nil.
 */
- (BOOL)loadFromFile:(nonnull NSString *)path error:(NSError **)error;

/*!
 *  Customise the styling of the base map (controller) using a JSON raw string. This string can be loaded
 *  from a resource file
 *  @param jsonString   A JSON style declaration according to the style reference.
 *  @param error        A pointer to an error object, you may specify nil.
 */
- (BOOL)setStyle:(nonnull NSString *)jsonString error:(NSError **)error;

/*!
 *  Adds an annotation view that will be placed at it's specific indoor location coordinate on the map.
 *  @param annotation The annotation view to be added.
 */
- (void)addAnnotationView:(nonnull IMAnnotationView *)annotation;

/*!
 *  Start navigate to location. If nil is passed it will stop the routing.
 *  @param location The destination point.
 */
- (void)navigateToLocation:(nullable IMLocation *)location;

/*!
 *  Updates the position of the user location.
 *  @param lon       The longitude of the location.
 *  @param lat       The latitude of the location.
 *  @param floor     The floor level of the location. Typically logical levels are used when generating maps for venues.
 *  @param accuracy  The accuracy (in meters) of the longitude/latitude
 */
- (void)setUserLocationLon:(double)lon lat:(double)lat floor:(int)floor accuracy:(float)accuracy;

/*!
 *  Updates the heading of the the user location.
 *  @param heading                 The heading relative to the geographic North Pole.
 *  @param accuracy                The heading accuracy in degrees.
 *  @param headingArbitraryNorth   The resulting value (double) represents the heading relative to an arbitrary North.
 */
- (void)setUserHeading:(double)heading accuracy:(double)accuracy headingArbitraryNorth:(double)headingArbitraryNorth;

@end

/*!
 *  This is the protocol used by IMMapViewController to inform the application of user and routing events.
 *
 *  All delegate methods are dispatched on the main dispatch queue.
 */
@protocol IMMapViewControllerDelegate <NSObject>

/*!
 *  This method is called when a user tapped on an annotation.
 *  @param sender      The IMMapViewController that is reporting the heading.
 *  @param annotation  The annotation that is tapped on.
 */
- (void)mapViewController:(nonnull IMMapViewController *)sender didTapAnnotation:(nonnull IMAnnotationView *)annotation;

/*!
 *  This method is called when a user tapped on the map.
 *  @param sender    The IMMapViewController that is reporting the heading.
 *  @param location  The tapped location on the map.
 */
- (void)mapViewController:(nonnull IMMapViewController *)sender didTapAtLocation:(nonnull IMLocation *)location;

/*!
 *  This method is called when the distance between the user and a destination set by navigateToLocation, is updated.
 *  @param sender    The IMMapViewController that is reporting the heading.
 *  @param distance  The distance in meters to the destination point.
 */
- (void)mapViewController:(nonnull IMMapViewController *)sender didUpdateRouteDistance:(double)distance;

@end
