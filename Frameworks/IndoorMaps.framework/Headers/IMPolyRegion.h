/*
 * (c) Philips Lighting, 2018.
 *   All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "IMLocation.h"
#import "IMRegion.h"

/*!
 *  Defines a geographical area that is described by a list of locations forming a closed shape.
 *  You can use instances of this class to define geofences at a specific location.
 */
@interface IMPolyRegion : IMRegion

/*!
 *  The floor level of the location. Typically logical levels are used when
 *  generating maps for venues.
 */
@property (nonatomic, readonly) int floorLevel;

/*!
 *  Returns an initialized polygon region object with a given set of location coordinates.
 *  The coordinates in this list are connected end-to-end from 0 up. The first
 *  and last points are connected to each other to create a closed shape.
 *
 *  @param locations    A list of IMLocation objects.
 *  @param floorLevel   The floor level of the location.
 */
- (nonnull instancetype)initWithLocations:(nonnull NSArray *)locations floorLevel:(int)floorLevel;

@end

