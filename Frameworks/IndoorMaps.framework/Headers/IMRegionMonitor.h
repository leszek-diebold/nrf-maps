/*
 * © Philips Lighting, 2018.
 *   All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <IndoorMaps/IMRegion.h>

@protocol IMRegionMonitorDelegate;

/*!
 *  Utility class to monitor distinct regions of interest (geofences).
 *  Crossing a regions boundary will generate location events on the delegate.
 */
@interface IMRegionMonitor : NSObject

/*!
 *  The delegate is used to communicate location events when the user enters or
 *  leaves monitored regions.
 */
@property (nullable, nonatomic, weak) id<IMRegionMonitorDelegate> delegate;

/*!
 *  The time in seconds after which an enter event is send when inside a region.
 */
@property (nonatomic, assign) double triggerTime;

/*!
 *  Add  a region to the receiver’s list of regions monitored.
 *  @param region   The region to be added.
 */
- (void)addRegion:(nonnull IMRegion *)region;

/*!
 *  Remove  a region from the receiver’s list of regions monitored.
 *  @param region   The region to be removed.
 */
- (void)removeRegion:(nonnull IMRegion *)region;

/*!
 *  Removes all regions from the receiver’s list of regions monitored.
 */
- (void)removeAllRegions;

/*!
 *  Updating the user location will generate location events when the user enters or leaves monitored regions.
 *  @param lon          The longitude of the location.
 *  @param lat          The latitude of the location.
 *  @param floorLevel   The floor level of the location.
 *  @param accuracy     The accuracy (in meters) of the longitude/latitude.
 */
- (void)setUserLocationLon:(double)lon lat:(double)lat floorLevel:(int)floorLevel accuracy:(float)accuracy;

@end

/*!
 *  This is the protocol used by IMRegionMonitor to inform the application of enter or leave
 *  events of monitored regions
 *
 *  All delegate methods are dispatched on the main dispatch queue.
 */
@protocol IMRegionMonitorDelegate <NSObject>

/*!
 *  This method is called when a region is entered with respect to the triggerTime.
 *  @param sender   The IMRegionMonitor that is reporting the didEnterRegion.
 *  @param region   The region that the user has entered.
 */
- (void)regionMonitor:(nonnull IMRegionMonitor *)sender didEnterRegion:(nonnull IMRegion *)region;

/*!
 *  This method is called when a region left.
 *  @param sender   The IMRegionMonitor that is reporting the didLeaveRegion.
 *  @param region   The region that the user has left.
 */
- (void)regionMonitor:(nonnull IMRegionMonitor *)sender didLeaveRegion:(nonnull IMRegion *)region;

@end
