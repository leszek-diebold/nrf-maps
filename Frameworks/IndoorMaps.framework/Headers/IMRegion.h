/*
 * © Philips Lighting, 2018.
 *   All rights reserved.
 */

#import <Foundation/Foundation.h>

/*! Virtual boundary of a real-world geographical area.
 *
 *  This class is abstract and you do not create instances of this class directly; instead,
 *  you instantiate subclasses that define specific types of regions.
 */
@interface IMRegion : NSObject

/*!
 *  The value that you specify and can use to identify this region inside your application.
 */
@property (nonatomic, strong) NSString *identifier;

/*!
 *  This function will return true if the location is inside the region, or false if it is not.
 *  @param lon          The longitude of the location.
 *  @param lat          The latitude of the location.
 *  @param floorLevel   The floor level of the location.
 */
- (BOOL)containsLocationWithLon:(double)lon lat:(double)lat floorLevel:(int)floorLevel;

@end
