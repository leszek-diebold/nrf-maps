//
//  Annotations.swift
//  IMSampleApp
//
//  Created by Leszek Mzyk on 09/01/2019.
//  Copyright © 2019 Signify. All rights reserved.
//

import Foundation
import UIKit

import IndoorMaps

class Annotations {
    var annotations = [ImageAnnotationView]()
    
    func createAnnotation(at location: IMLocation) -> ImageAnnotationView {
        let annotation = ImageAnnotationView(imageName: "img_annotation", location: location)
        annotation.metricSize = CGSize(width: 1.0, height: 1.0)
        annotation.counterRotate = true
        annotations.append(annotation)
        return annotation
    }
    
    func removeAnnotations() {
        for annotation in annotations {
            annotation.removeFromSuperview()
        }
        annotations.removeAll()
    }
}
