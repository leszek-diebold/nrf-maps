//
//  WorkMode.swift
//  IMSampleApp
//
//  Created by Leszek Mzyk on 09/01/2019.
//  Copyright © 2019 Signify. All rights reserved.
//

import Foundation

enum WorkMode {
    case positioning
    case regions
}
