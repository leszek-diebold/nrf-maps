//
//  PositioningMode.swift
//  IMSampleApp
//
//  Created by Leszek Mzyk on 02/01/2019.
//  Copyright © 2019 Signify. All rights reserved.
//

import Foundation

enum PositioningMode {
    case manual
    case automatic
}
