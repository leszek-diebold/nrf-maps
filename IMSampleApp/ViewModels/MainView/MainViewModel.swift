//
//  MainViewModel.swift
//  IMSampleApp
//
//  Created by Krygu on 14/12/2018.
//  Copyright © 2018 Signify. All rights reserved.
//

import Foundation
import IndoorMaps
import IndoorPositioning

class MainViewModel: NSObject {
    
    private var viewDelegate: MainViewDelegate
    private(set) var regionData: RegionData!
    private(set) var regionMonitor: IMRegionMonitor! {
        didSet {
            regionMonitor.delegate = self
            for region in regionData.allRegions {
                regionMonitor.add(region)
            }
        }
    }
    
    private(set) var indoorPositioning: IPIndoorPositioning! {
        didSet {
            indoorPositioning.delegate = self
            indoorPositioning.mode = .default
            indoorPositioning.headingOrientation = .portrait
            indoorPositioning.configuration = Configuration.eu.rawValue
        }
    }
    
    public var workMode: WorkMode = WorkMode.positioning
    
    public private(set) var positioningMode: PositioningMode = PositioningMode.manual {
        didSet {
            switch positioningMode {
            case .automatic:
                indoorPositioning.start()
            case .manual:
                indoorPositioning.stop()
            }
        }
    }
    
    init(viewDelegate: MainViewDelegate) {
        self.viewDelegate = viewDelegate
        super.init()
        prepareRegions()
        preparePositioning()
    }
    
    private func prepareRegions() {
        regionData = RegionData()
        regionMonitor = IMRegionMonitor()
    }
    
    private func preparePositioning() {
        indoorPositioning = IPIndoorPositioning.sharedInstance()
    }
    
    func togglePostioningMode() -> PositioningMode {
        positioningMode = positioningMode == PositioningMode.automatic ?
            PositioningMode.manual :
            PositioningMode.automatic
        return positioningMode
    }
    
    func setManualLocation(at location:IMLocation) {
        let lat = location.lat
        let lon = location.lon
        let floor:Int32 = 0
        viewDelegate.setUserLocation(lon: lon, lat: lat, floor: floor, accuracy: 0.1)
        regionMonitor.setUserLocationLon(lon, lat: lat, floorLevel: floor, accuracy: 0.1)
        regionMonitor.setUserLocationLon(lon, lat: lat, floorLevel: floor, accuracy: 0.1)
    }

    
    func addPositionToRegion(at location: IMLocation) -> Bool {
        return regionData.addPositionToRegion(at: location)
    }
    
    func startNewRegion () {
        finishRegion()
        regionData.startRegion()
    }
    
    func finishRegion() {
        if let region = regionData.finishRegion() {
            regionMonitor.add(region)
        }
    }
    
    func clearRegions() {
        regionMonitor.removeAllRegions()
        regionData.clear()
        viewDelegate.clearRegions()
    }
    func getRegionsData() -> Array<Any> {
        return regionData.getRawRegions()
    }
}

// MARK: IMRegionMonitorDelegate
extension MainViewModel: IMRegionMonitorDelegate {
    
    func regionMonitor(_ sender: IMRegionMonitor, didEnter region: IMRegion) {
        print("Entered Region: \(region.identifier ?? "")")
        viewDelegate.setRegionInfo("Entered \(region.identifier ?? "")")
    }
    
    func regionMonitor(_ sender: IMRegionMonitor, didLeave region: IMRegion) {
        print("Left Region: \(region.identifier ?? "")")
        viewDelegate.setRegionInfo("Leave \(region.identifier ?? "")")
    }
    
}

// MARK: IPIndoorPositioningDelegate
extension MainViewModel: IPIndoorPositioningDelegate {
    
    func indoorPositioning(_ indoorPositioning: IPIndoorPositioning!, didUpdateHeading heading: [AnyHashable : Any]!) {
        guard
            let headingNorth = heading[kIPHeadingDegrees] as? Double,
            let headingArbitraryNorth = heading[kIPHeadingArbitraryNorthDegrees] as? Double,
            let headingAccuracy = heading[kIPHeadingAccuracy] as? Double
            else {
                return
        }
        viewDelegate.setUserHeading(headingNorth: headingNorth, accuracy: headingAccuracy, headingArbitraryNorth: headingArbitraryNorth)
    }
    
    func indoorPositioning(_ indoorPositioning: IPIndoorPositioning!, didUpdateLocation location: [AnyHashable : Any]!) {
        guard
            let lat = location[kIPLocationLatitude] as? Double,
            let lon = location[kIPLocationLongitude] as? Double
            else {
                return
        }
        let floor:Int32 = 0
        viewDelegate.setUserLocation(lon: lon, lat: lat, floor: floor, accuracy: 0.3)
        regionMonitor.setUserLocationLon(lon, lat: lat, floorLevel: floor, accuracy: 0.3)
    }
    
    func indoorPositioning(_ indoorPositioning: IPIndoorPositioning!, didFailWithError error: Error!) {
        NSLog("Error indoor positioning: \(error.localizedDescription)")
    }
    
}
