//
//  RegionData.swift
//  IMSampleApp
//
//  Created by Krygu on 20/11/2018.
//  Copyright © 2018 Signify. All rights reserved.
//

import Foundation
import IndoorMaps

class RegionData {
    
    //MARK: INTIAL REGIONS DATA
    
    private let region1Locations = [
        IMLocation(lon: -74.00091439567056, lat: 40.758542766077085, floorLevel: 0),
        IMLocation(lon: -74.0009843399559, lat: 40.75844769380767, floorLevel: 0),
        IMLocation(lon: -74.00100722195343, lat: 40.75853007652404, floorLevel: 0)
    ]
    private let region2Locations = [
        IMLocation(lon: -74.0009843399559, lat: 40.75844769380767, floorLevel: 0),
        IMLocation(lon: -74.00111030020247, lat: 40.758498773509054, floorLevel: 0),
        IMLocation(lon: -74.00100722195343, lat: 40.75853007652404, floorLevel: 0)
    ]
    private let region3Locations = [
        IMLocation(lon: -74.00111030020247, lat: 40.758498773509054, floorLevel: 0),
        IMLocation(lon: -74.00104041080633, lat: 40.758595114110555, floorLevel: 0),
        IMLocation(lon: -74.00100722195343, lat: 40.75853007652404, floorLevel: 0)
    ]
    private let region4Locations = [
        IMLocation(lon: -74.00104041080633, lat: 40.758595114110555, floorLevel: 0),
        IMLocation(lon: -74.00091439567056, lat: 40.758542766077085, floorLevel: 0),
        IMLocation(lon: -74.00100722195343, lat: 40.75853007652404, floorLevel: 0)
    ]
    
    private let itemLocations = [IMLocation(lon: -74.00099306393491, lat: 40.758545493592436, floorLevel: 0)]
    
    private let region1: IMPolyRegion
    private let region2: IMPolyRegion
    private let region3: IMPolyRegion
    private let region4: IMPolyRegion
    
    private var definedRawRegions: [RawRegion] = []
    private var definedRegions: [IMPolyRegion] = []
    private var newRegion:RawRegion?
    
    //MARK: Getters
    var allRegions: [IMPolyRegion] {
        get {
            return definedRegions.count > 0
                ? definedRegions
                : [region1, region2, region3, region4]
        }
    }
    var allLocations: [IMLocation] {
        get {
            return definedRawRegions.count > 0
                ? getLocationsFromRawRegions()
                : region1Locations + region2Locations + region3Locations + region4Locations + itemLocations
        }
    }
    
    private func getLocationsFromRawRegions() -> [IMLocation] {
        return []
    }
    
    //MARK: Initializer
    init() {
        region1 = IMPolyRegion(locations: region1Locations, floorLevel: 0)
        region1.identifier = "First region"
        region2 = IMPolyRegion(locations: region2Locations, floorLevel: 0)
        region2.identifier = "Second region - payement"
        region3 = IMPolyRegion(locations: region3Locations, floorLevel: 0)
        region3.identifier = "Third region"
        region4 = IMPolyRegion(locations: region4Locations, floorLevel: 0)
        region4.identifier = "Fourth region - coupon"
    }
    
    func clear() {
        definedRawRegions.removeAll()
        definedRegions.removeAll()
        newRegion = nil
    }
    
    func startRegion() {
        newRegion = RawRegion(withName: "region \(definedRawRegions.count+1)", withId: definedRawRegions.count)
    }
    
    func finishRegion() -> IMRegion? {
        var createdRegion:IMRegion? = nil
        if let regionToSave = newRegion {
            createdRegion = saveRawRegion(regionToSave)
        }
        newRegion = nil
        return createdRegion
    }
    
    private func saveRawRegion(_ region: RawRegion)  -> IMRegion {
        let polyRegion = IMPolyRegion(locations: region.locations, floorLevel: 0)
        polyRegion.identifier = region.name
        
        definedRegions.insert(polyRegion, at: definedRegions.count)
        definedRawRegions.insert(region, at: definedRawRegions.count)
        
        return polyRegion
    }
    
    func addPositionToRegion(at location: IMLocation) -> Bool {
        return newRegion?.addLocation(location) ?? false
    }
    
    func getRawRegions() -> Array<Any> {
        var regions:Array<Any> = []
        for rawRegion in definedRawRegions {
            regions.insert(rawRegion.getSerializable(), at: regions.count)
        }
        return regions
    }
}


class RawRegion {
    let name:String
    let id:Int
    private(set) var locations:[IMLocation] = []
    
    init(withName name:String, withId id:Int) {
        self.name = name
        self.id = id
    }
    
    func addLocation(_ location:IMLocation) -> Bool {
        locations.insert(location, at:locations.count)
        return true
    }
    func getSerializable() -> Dictionary<String, Any> {
        var points:Array<Any> = []
        for location in locations {
            let point = [
                "longitude": location.lon,
                "latitude": location.lat
            ]
            points.insert(point, at: points.count)
        }

        let dict:Dictionary<String, Any> = [
            "id": id,
            "locationName": name,
            "pictureName": "21.png",
            "points" : points
        ]
        
        return dict
    }
}

