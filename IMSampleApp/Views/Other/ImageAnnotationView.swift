/*
 * (c) Philips Lighting, 2018.
 *   All rights reserved.
 */

import IndoorMaps

class ImageAnnotationView: IMAnnotationView
{
    init(imageName:String, location:IMLocation)
    {
        let image = UIImage(named: imageName)
        super.init(frameSize:(image?.size)!, location: location)
        self.addSubview(UIImageView(image: image))
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
}

