/*
 * © Philips Lighting, 2018.
 *   All rights reserved.
 */

import UIKit
import IndoorMaps
import IndoorPositioning

class MainViewController: UIViewController {

    // MARK: Properties
    @IBOutlet weak var regionInfoLabel: UILabel!
    @IBOutlet weak var togglePositioningModeButton: UIButton!
    @IBOutlet weak var userPositionLabel: UILabel!
    @IBOutlet weak var annotationPositionLabel: UILabel!
    @IBOutlet weak var locationAreaInfo: UIView!
    @IBOutlet weak var regionsInfo: UIView! {
        didSet {
            regionsInfo.isHidden = true
        }
    }
    
    private var annotations: Annotations = Annotations()
    private var viewModel: MainViewModel!
    private var mapController: IMMapViewController!
    private var regionsController: RegionsViewController!
    
    @IBAction func togglePositioningMode(_ sender: UIButton) {
        let positioningMode = viewModel.togglePostioningMode()
        updatePositioningModeTitle(positioningMode)
    }
    
    @IBAction func toggleLocationArea(_ sender: UIButton) {
        regionsInfo.isHidden = !regionsInfo.isHidden
        viewModel.workMode = regionsInfo.isHidden ? .positioning : .regions
        if (regionsInfo.isHidden) {
            viewModel.finishRegion()
        }
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = MainViewModel(viewDelegate: self)
        regionsController.viewModel = viewModel
        updatePositioningModeTitle(viewModel.positioningMode)
        do {
            let styleJson = try String(contentsOfFile: Bundle.main.bundlePath + "/Style.json")
            try mapController?.setStyle(styleJson)
            try mapController?.load(fromFile:  Bundle.main.bundlePath + "/map-zebra.bin")

            mapController?.view.clipsToBounds = true
            mapController?.delegate = self
        } catch {
            print("Failed to init the map controller: \(error.localizedDescription)")
        }
    }
    
    private func updatePositioningModeTitle(_ positioningMode: PositioningMode) {
        let title = positioningMode == .automatic ? "Automatic Location" : "Manual Location"
        togglePositioningModeButton.setTitle(title, for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        for location in viewModel.regionData.allLocations {
            addAnnotation(at: location)
        }
    }

    private func addAnnotation(at location: IMLocation) {
        let annotation = annotations.createAnnotation(at: location)
        mapController?.add(annotation)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch (segue.identifier) {
        case "MAP_CONTROLLER":
            mapController = segue.destination as? IMMapViewController
        case "REGIONS_CONTROLLER":
            regionsController = segue.destination as? RegionsViewController
        default:
            break
        }
    }
    
    
    
    private func setAnnotationPositionLabel(using location: IMLocation) {
        let lonStr = String(format: "%.7f", location.lon)
        let latStr = String(format: "%.7f", location.lat)
        annotationPositionLabel.text = "lon: \(lonStr)\nlat: \(latStr)"
    }
    
    private func setUserPositionLabel(lon: Double, lat: Double) {
        let lonStr = String(format: "%.7f", lon)
        let latStr = String(format: "%.7f", lat)
        userPositionLabel.text = "lon: \(lonStr)\nlat: \(latStr)"
    }
}

// MARK: IMMapViewControllerDelegate
extension MainViewController: IMMapViewControllerDelegate {
    
    func mapViewController(_ sender: IMMapViewController, didTapAt location: IMLocation) {
        if (viewModel.workMode == .positioning){
            if (viewModel.positioningMode == .manual) {
                view.layoutIfNeeded()
                viewModel.setManualLocation(at: location)
            }
        } else {
            let positionAdded = viewModel.addPositionToRegion(at: location)
            if (positionAdded) {
                addAnnotation(at: location)
            }
        }
       
    }
    
    func mapViewController(_ sender: IMMapViewController, didTapAnnotation annotation: IMAnnotationView) {
        setAnnotationPositionLabel(using: annotation.location)
        if (viewModel.workMode == .regions){
            let _ = viewModel.addPositionToRegion(at: annotation.location)
        }
    }
    
    func mapViewController(_ sender: IMMapViewController, didUpdateRouteDistance distance: Double) {
        print("Updated distance: \(distance)")
    }
    
}

//MARK: MainViewDelegate
extension MainViewController: MainViewDelegate {
    
    func setUserHeading(headingNorth: Double, accuracy: Double, headingArbitraryNorth: Double) {
        mapController?.setUserHeading(headingNorth, accuracy: accuracy, headingArbitraryNorth: headingArbitraryNorth)

    }
    
    func setUserLocation(lon: Double, lat: Double, floor: Int32, accuracy: Float) {
        mapController?.setUserLocationLon(lon, lat: lat, floor: floor, accuracy: accuracy)
        setUserPositionLabel(lon: lon, lat: lat)
    }
    
    func setRegionInfo(_ info: String) {
        regionInfoLabel.text = info
    }
    
    func clearRegions() {
        annotations.removeAnnotations()
    }
}
