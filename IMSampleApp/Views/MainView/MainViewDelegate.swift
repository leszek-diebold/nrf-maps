//
//  MainViewDelegate.swift
//  IMSampleApp
//
//  Created by Krygu on 14/12/2018.
//  Copyright © 2018 Signify. All rights reserved.
//

import Foundation
import IndoorPositioning

protocol MainViewDelegate: class {
    
    func setUserHeading(headingNorth: Double, accuracy: Double, headingArbitraryNorth: Double)
    func setUserLocation(lon: Double, lat: Double, floor: Int32, accuracy: Float)
    func setRegionInfo(_ info: String)
    func clearRegions()
}
