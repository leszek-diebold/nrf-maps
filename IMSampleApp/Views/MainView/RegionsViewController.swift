//
//  RegionsViewController.swift
//  IMSampleApp
//
//  Created by Leszek Mzyk on 03/01/2019.
//  Copyright © 2019 Signify. All rights reserved.
//

import Foundation

import UIKit
import IndoorMaps
import IndoorPositioning

class RegionsViewController: UIViewController {
    public var viewModel: MainViewModel!
    
    @IBAction func clearAll(_ sender: UIButton) {
        viewModel.clearRegions()
    }
    
    @IBAction func startNewRegion(_ sender: UIButton) {
        viewModel.startNewRegion()
    }
    
    @IBAction func finishRegion(_ sender: UIButton) {
        viewModel.finishRegion()
    }
    
    @IBAction func copyRegions(_ sender:UIButton) {
        
        let data = viewModel.getRegionsData()
        let jsonData = try! JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
        UIPasteboard.general.string = jsonString! as String
    }
}
